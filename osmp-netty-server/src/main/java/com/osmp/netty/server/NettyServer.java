/*   
* Project: OSMP
* FileName: NettyServer.java
* version: V1.0
*/
package com.osmp.netty.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.osmp.intf.define.server.Server;
import com.osmp.netty.exception.ServerException;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Description:netty 服务器
 * 
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:13:14上午10:51:30
 */
public class NettyServer implements Server, ApplicationContextAware{
	private Logger logger = LoggerFactory.getLogger(NettyServer.class);
	
	private ApplicationContext applicationContext;
	
	private int bossGroupThreads;

	private int workerGroupThreads;

	private int backlogSize;
	
	private int port;

	private Channel channel;
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;

	@Override
	public void start() {
		bossGroup = new NioEventLoopGroup(bossGroupThreads);
		workerGroup = new NioEventLoopGroup(workerGroupThreads);
		ServerBootstrap serverBootstrap = new ServerBootstrap();
		serverBootstrap
			.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.option(ChannelOption.SO_BACKLOG, backlogSize)
			.childOption(ChannelOption.SO_KEEPALIVE, true)
			.childOption(ChannelOption.TCP_NODELAY, true)
			.childHandler(applicationContext.getBean(NettyServerChannelInitializer.class));
		try {
			channel = serverBootstrap.bind(port).sync().channel();
			logger.debug("netty server started and port is " + port + " ...");
		} catch (final InterruptedException ex) {
			throw new ServerException(Server.SYSTEM_MESSAGE_ID, ex);
		}

	}

	@Override
	public void stop() {
		if (null == channel) {
			throw new RuntimeException("channel is null");
		}
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		bossGroup = null;
		workerGroup = null;
		channel = null;
		logger.debug("netty server is stoped...");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	/**
	 * @return the bossGroupThreads
	 */
	public int getBossGroupThreads() {
		return bossGroupThreads;
	}

	/**
	 * @param bossGroupThreads the bossGroupThreads to set
	 */
	public void setBossGroupThreads(int bossGroupThreads) {
		this.bossGroupThreads = bossGroupThreads;
	}

	/**
	 * @return the backlogSize
	 */
	public int getBacklogSize() {
		return backlogSize;
	}

	/**
	 * @param backlogSize the backlogSize to set
	 */
	public void setBacklogSize(int backlogSize) {
		this.backlogSize = backlogSize;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the workerGroupThreads
	 */
	public int getWorkerGroupThreads() {
		return workerGroupThreads;
	}

	/**
	 * @param workerGroupThreads the workerGroupThreads to set
	 */
	public void setWorkerGroupThreads(int workerGroupThreads) {
		this.workerGroupThreads = workerGroupThreads;
	}

}
