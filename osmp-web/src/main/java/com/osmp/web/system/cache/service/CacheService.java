package com.osmp.web.system.cache.service;

import java.util.Map;

import com.osmp.web.system.cache.entity.CacheDefined;

public interface CacheService {

    void open(String serverIp);

    void close(String serverIp);

    String getCacheInfo(String serverIp);

    String getCacheList(String serverIp);

    String getCacheItem(Map<String, String> params, String serverIp);

    String getCacheById(String cacheId, String serverIp);

    String updateCache(CacheDefined cacheDefined, String serverIp);

    String deleteCache(String key, String serverIp);

}
