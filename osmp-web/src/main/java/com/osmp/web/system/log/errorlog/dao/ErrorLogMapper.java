package com.osmp.web.system.log.errorlog.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.log.errorlog.entity.ErrorLog;

/**
 * Description:
 * 
 * @author: zhangjunming
 * @date: 2014年10月13日 下午17:22:09
 */
public interface ErrorLogMapper extends BaseMapper<ErrorLog> {

}
