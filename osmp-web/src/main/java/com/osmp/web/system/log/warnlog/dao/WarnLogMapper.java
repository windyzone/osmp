package com.osmp.web.system.log.warnlog.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.log.warnlog.entity.WarnLog;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年10月13日 下午3:14:56
 */

public interface WarnLogMapper extends BaseMapper<WarnLog> {

}
