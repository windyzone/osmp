 /*   
 * Project: OSMP
 * FileName: NettyClient.java
 * version: V1.0
 */
package com.osmp.netty.client;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.osmp.intf.define.server.Client;
import com.osmp.intf.define.server.Request;
import com.osmp.intf.define.server.Response;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 下午4:17:58上午10:51:30
 */
public class NettyClient implements Client, ApplicationContextAware{
	
	private ApplicationContext applicationContext;
	private EventLoopGroup workerGroup;
	private Channel channel;
	private String host;
	private int port;
	
	@Override
	public void connect() {
		workerGroup = new NioEventLoopGroup(1);
		Bootstrap bootstrap = new Bootstrap();
		bootstrap
			.group(workerGroup)
			.channel(NioSocketChannel.class)
			.option(ChannelOption.SO_KEEPALIVE, true)
			.option(ChannelOption.TCP_NODELAY, true)
			.handler(applicationContext.getBean(ClientChannelInitializer.class));
		channel = bootstrap.connect(host, port).syncUninterruptibly().channel();
		
	}

	@Override
	public Response sent(Request request) {
		channel.writeAndFlush(request);
		return applicationContext.getBean(ClientChannelInitializer.class).getResponse(request.getMsgId());
	}

	@Override
	public InetSocketAddress getRemoteAddress() {
		SocketAddress remoteAddress = channel.remoteAddress();
		if (!(remoteAddress instanceof InetSocketAddress)) {
			throw new RuntimeException("Get remote address error, should be InetSocketAddress");
		}
		return (InetSocketAddress) remoteAddress;
	}

	@Override
	public void close() {
		if (null == channel) {
			throw new RuntimeException();
		}
		workerGroup.shutdownGracefully();
		channel.closeFuture().syncUninterruptibly();
		workerGroup = null;
		channel = null;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
}
